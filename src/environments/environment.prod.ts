export const environment = {
  production: true,
  auth: {
    clientID: 'ld2n3EeiulLl6wlM5Rxc4S5QhtOx3Xnr',
    domain: 'sgrentals.auth0.com',
    responseType: 'token id_token',
    audience: 'https://sgrentals.auth0.com/userinfo',
    redirectUri: 'http://localhost:4200/callback',
    scope: 'openid profile'
  }
};

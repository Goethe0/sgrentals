import { Component, OnInit, Input } from '@angular/core'
import { CarData } from '../domain/cars'
import { RentData } from '../domain/rent'
import { UserData } from '../domain/user'

@Component({
  selector: 'app-rent-checkout',
  templateUrl: './rent-checkout.component.html',
  styleUrls: ['./rent-checkout.component.scss']
})
export class RentCheckoutComponent implements OnInit {

  @Input() carData: CarData
  @Input() userData: UserData
  @Input() rentData: RentData
  
  cbReceiveNews
  rbTravelReason

  numberOfInstallments = Array(6).fill(0).map((x, i) => 2 + i)
  numberOfMonths = Array(12).fill(0).map((x, i) => 1 + i)
  numberOfYears = Array(60).fill(0).map((x, i) => 2030 - i)

  constructor() { }

  ngOnInit() {
  }
}

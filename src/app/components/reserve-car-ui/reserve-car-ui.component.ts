import { DataService } from './../../services/data.service'
import { Component, OnInit } from '@angular/core'
import { Router } from "@angular/router"
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Observable } from 'rxjs'
import { ReserveCarService } from '../../services/reserve-car.service'

@Component({
  selector: 'app-reserve-car-ui',
  templateUrl: './reserve-car-ui.component.html',
  styleUrls: ['./reserve-car-ui.component.scss']
})
export class ReserveCarUIComponent implements OnInit {

  form: FormGroup
  reserveCar: Observable<ReserveCarService>

  withdrawalOrigin: string
  returnDestiny: string
  withdrawalDate: string
  withdrawalHour: string
  returnDate: string
  returnHour: string

  constructor(
    private dataService: DataService,
    private router: Router
  ) {

  }

  ngOnInit() {

  }

  advanceToCarSelection() {
    this.dataService.setRentData({
      returnDate: new Date(7, 6, 2012, 10),
      returnDestiny: 'Aeroporto 1',
      withdrawalDate: new Date(10, 6, 2012, 18),
      withdrawalOrigin: 'Aeroporto 2'
    })

    this.router.navigate(['cars'])
  }
}

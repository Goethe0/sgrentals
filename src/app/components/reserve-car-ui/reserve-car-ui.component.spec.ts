import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReserveCarUIComponent } from './reserve-car-ui.component';

describe('ReserveCarUIComponent', () => {
  let component: ReserveCarUIComponent;
  let fixture: ComponentFixture<ReserveCarUIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReserveCarUIComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReserveCarUIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

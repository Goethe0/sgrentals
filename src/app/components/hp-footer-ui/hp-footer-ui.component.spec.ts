import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HpFooterUIComponent } from './hp-footer-ui.component';

describe('HpFooterUIComponent', () => {
  let component: HpFooterUIComponent;
  let fixture: ComponentFixture<HpFooterUIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HpFooterUIComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HpFooterUIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RentalStatePricesUIComponent } from './rental-state-prices-ui.component';

describe('RentalStatePricesUIComponent', () => {
  let component: RentalStatePricesUIComponent;
  let fixture: ComponentFixture<RentalStatePricesUIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RentalStatePricesUIComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RentalStatePricesUIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

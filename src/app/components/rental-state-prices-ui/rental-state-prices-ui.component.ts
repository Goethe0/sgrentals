import { Component, OnInit, Input } from '@angular/core';

interface RentalStateLocation
{
  cityName: string
  countryInitials: string
  currency: string
  pricePerDay: string
}

export interface RentalStatePriceData
{
  countryName: string
  cityPriceList: RentalStateLocation[]
}

@Component({
  selector: 'app-rental-state-prices-ui',
  templateUrl: './rental-state-prices-ui.component.html',
  styleUrls: ['./rental-state-prices-ui.component.scss']
})

export class RentalStatePricesUIComponent implements OnInit {

  @Input() countryName: string;
  @Input() cityPriceList: RentalStateLocation[];

  constructor() { }

  ngOnInit() {
  }

}

import { UserData } from './../../domain/user';
import { DataService } from './../../services/data.service';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login-button',
  templateUrl: './login-button.component.html',
  styleUrls: ['./login-button.component.scss']
})
export class LoginButtonComponent implements OnInit {

  userData: UserData;

  constructor(public authService: AuthService, private dataService: DataService) { }

  ngOnInit() {
    this.dataService.userData.subscribe(userData => this.userData = userData);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsletterSubscriptionUIComponent } from './newsletter-subscription-ui.component';

describe('NewsletterSubscriptionUIComponent', () => {
  let component: NewsletterSubscriptionUIComponent;
  let fixture: ComponentFixture<NewsletterSubscriptionUIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsletterSubscriptionUIComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsletterSubscriptionUIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hp-header',
  templateUrl: './hp-header.component.html',
  styleUrls: ['./hp-header.component.scss']
})
export class HpHeaderComponent implements OnInit {

  constructor(public authService: AuthService) { }

  ngOnInit() {
    this.authService.handleAuthentication();
  }
}

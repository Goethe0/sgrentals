import { CarListComponent } from './car-list/car-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { RentCheckoutComponent } from './rent-checkout/rent-checkout.component';
import { CarsListComponent } from './components/cars-list/cars-list.component';
import { AuthCallbackComponent } from './auth-callback/auth-callback.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { TestComponent} from './test/test.component';
import {CarSearchComponent} from './car-search/car-search.component';


import { AuthGuard } from "./auth/auth.guard";


const routes: Routes = [
  { path: 'car-search', component: CarSearchComponent},
  { path: 'checkout', component: RentCheckoutComponent},
  { path: 'cars-list/id', component: CarsListComponent},
  { path: 'cars', component: CarListComponent},
  { path: 'home', component: HomeComponent},
  { path: 'callback', component: AuthCallbackComponent},
  { path: 'my-account', component: MyAccountComponent, canActivate: [AuthGuard]},
  { path: '', pathMatch: 'full', redirectTo: 'home'},
  { path: 'test', component: TestComponent}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }

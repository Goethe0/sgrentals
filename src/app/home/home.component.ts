import { Component, OnInit } from '@angular/core';
import { RentalStatePriceData } from '../components/rental-state-prices-ui/rental-state-prices-ui.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  offers: RentalStatePriceData[] = [
    {
      countryName: 'Estados Unidos',
      cityPriceList: [
        {
          cityName: 'Miami',
          countryInitials: 'EUA',
          currency: 'U$',
          pricePerDay: '15.14'
        },
        {
          cityName: 'Orlando',
          countryInitials: 'EUA',
          currency: 'U$',
          pricePerDay: '15.14'
        },
        {
          cityName: 'Tampa',
          countryInitials: 'EUA',
          currency: 'U$',
          pricePerDay: '15.14'
        },
        {
          cityName: 'Las Vegas',
          countryInitials: 'EUA',
          currency: 'U$',
          pricePerDay: '18.87'
        }
      ]
    },
    {
      countryName: 'Europa',
      cityPriceList: [
        {
          cityName: 'Madri',
          countryInitials: 'Espanha',
          currency: 'U$',
          pricePerDay: '24.53'
        },
        {
          cityName: 'Paris',
          countryInitials: 'France',
          currency: 'U$',
          pricePerDay: '32.00'
        },
        {
          cityName: 'Frankfurt',
          countryInitials: 'Alemanha',
          currency: 'U$',
          pricePerDay: '29.17'
        },
        {
          cityName: 'Roma',
          countryInitials: 'Italia',
          currency: 'U$',
          pricePerDay: '28.10'
        }
      ]
    },
    {
      countryName: 'America Latina',
      cityPriceList: [
        {
          cityName: 'San Tiago',
          countryInitials: 'Chile',
          currency: 'U$',
          pricePerDay: '42.25'
        },
        {
          cityName: 'Buenos Aires',
          countryInitials: 'Arg',
          currency: 'U$',
          pricePerDay: '52.00'
        },
        {
          cityName: 'Montevideu',
          countryInitials: 'Uru',
          currency: 'U$',
          pricePerDay: '57.37'
        },
        {
          cityName: 'Lima',
          countryInitials: 'Peru',
          currency: 'U$',
          pricePerDay: '35.57'
        }
      ]
    },
  ]

  constructor() { }

  ngOnInit() {
  }

}

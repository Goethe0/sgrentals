import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-car-search',
    templateUrl: './car-search.component.html',
    styleUrls: ['./car-search.component.scss']
})
export class CarSearchComponent implements OnInit {

    btnSortClass1: string = 'btn-selected';
    btnSortClass2: string = 'btn-not-selected';

    searchModel: string = '';

    searchArray:Array<any> = [];

    constructor() {
    }

    onSearchModelChange() {
        console.log("this");
        console.log(this.searchModel);
        this.searchArray = [];

        this.mockdataAll.forEach((element) => {
           if (element.car.car_model.toLowerCase().indexOf(this.searchModel.toLowerCase()) > -1) {
               this.searchArray.push(element);
           }
        });

        this.mockdata = this.searchArray;
    }


    onSortFunction(event) {
        console.log("click");
        console.log(event);
        if (event === 1) {
            this.sortMockData(event);
            this.btnSortClass1 = 'btn-selected';
            this.btnSortClass2 = 'btn-not-selected';
        } else if (event ===2) {
            this.sortMockData(event);
            this.btnSortClass1 = 'btn-not-selected';
            this.btnSortClass2= 'btn-selected';
        }
    }

    sortMockData(event) {
        if (event === 1){
            this.mockdata = this.mockdata.sort(this.compareDesc);
        } else if (event === 2) {
            this.mockdata = this.mockdata.sort(this.compareAsc);
        }

    }

    compareAsc(a, b){
        if (a.car.price_total < b.car.price_total)
            return -1;
        if (a.car.price_total > b.car.price_total)
            return 1;
        return 0;
    }

    compareDesc(a, b){
        if (a.car.price_total > b.car.price_total)
            return -1;
        if (a.car.price_total < b.car.price_total)
            return 1;
        return 0;
    }

    searchName(event) {

    }


    mockdataAll = [
        {
            'company_name': 'Hertz',
            'company_logo': 'https://main-brightonpride.s3.amazonaws.com/wp-content/uploads/2016/11/Hertz-400x200.jpg',
            'score': 4,
            'score_text': 'Muito Bom',
            'location': 'Balcão de Atendimento',
            'car': {
                'car_model': 'Chevrolet Spin',
                'car_category': 'Categoria Minivan',
                'image': 'http://d2pa5gi5n2e1an.cloudfront.net/global/images/product/cars/Chevrolet_Spin/Chevrolet_Spin_L_1.jpg',
                'engine' : '1.8',
                'air' : 1,
                'shift': 'Manual',
                'electric_lock': 1,
                'abs': 1,
                'radio': 'CD Player USB',
                'passengers': 7,
                'electric_window': 1,
                'airbag': 1,
                'doors': 4,
                'trunk': '400 litros',
                'price_total': 230.65,
                'price_day': 76.87
            },
            'package_includes': [
                'KM Livre',
                'Proteção Pessoal',
                'Proteção contra colisão e dano',
                'Taxas locais',
                '1 Motorista Adicional'
            ],
            'offer_msg': 'Um condutor adicional incluso'
        },
        {
            'company_name': 'Hertz',
            'company_logo': 'https://moveuptogether.ca/wp-content/uploads/2006/01/hertz.jpg',
            'score': 4,
            'score_text': 'Muito Bom',
            'location': 'Balcão de Atendimento',
            'car': {
                'car_model': 'Renault Sandero',
                'car_category': 'Categoria Minivan',
                'image': 'https://www.blogauto.com.br/wp-content/2007/09/sandero1.jpg',
                'engine' : '1.8',
                'air' : 1,
                'shift': 'Manual',
                'electric_lock': 1,
                'abs': 1,
                'radio': 'CD Player USB',
                'passengers': 7,
                'electric_window': 1,
                'airbag': 1,
                'doors': 4,
                'trunk': '400 litros',
                'price_total': 230.63,
                'price_day': 76.87
            },
            'package_includes': [
                'KM Livre',
                'Proteção Pessoal',
                'Proteção contra colisão e dano',
                'Taxas locais',
                '1 Motorista Adicional'
            ],
            'offer_msg': 'Um condutor adicional incluso'
        },
        {
            'company_name': 'Hertz',
            'company_logo': 'https://moveuptogether.ca/wp-content/uploads/2006/01/hertz.jpg',
            'score': 4,
            'score_text': 'Muito Bom',
            'location': 'Balcão de Atendimento',
            'car': {
                'car_model': 'Chevrolet Onix',
                'car_category': 'Categoria Minivan',
                'image': 'https://www.chavesnamao.com.br/public/uploads/veiculo_modelo/327/327_fipe_1.jpg',
                'engine' : '1.8',
                'air' : 1,
                'shift': 'Manual',
                'electric_lock': 1,
                'abs': 1,
                'radio': 'CD Player USB',
                'passengers': 7,
                'electric_window': 1,
                'airbag': 1,
                'doors': 4,
                'trunk': '400 litros',
                'price_total': 230.62,
                'price_day': 76.87
            },
            'package_includes': [
                'KM Livre',
                'Proteção Pessoal',
                'Proteção contra colisão e dano',
                'Taxas locais',
                '1 Motorista Adicional'
            ],
            'offer_msg': 'Um condutor adicional incluso'
        },
        {
            'company_name': 'Hertz',
            'company_logo': 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQspVQ_Jy2Ap5oS7wswiV34O0ZOkBvK2FjCiSsNWsDrEVWzi1js',
            'score': 4,
            'score_text': 'Muito Bom',
            'location': 'Balcão de Atendimento',
            'car': {
                'car_model': 'Peugeot 5008',
                'car_category': 'Categoria Minivan',
                'image': 'http://d2pa5gi5n2e1an.cloudfront.net/global/images/product/cars/Peugeot_5008/Peugeot_5008_L_1.jpg',
                'engine' : '1.8',
                'air' : 1,
                'shift': 'Manual',
                'electric_lock': 1,
                'abs': 1,
                'radio': 'CD Player USB',
                'passengers': 7,
                'electric_window': 1,
                'airbag': 1,
                'doors': 4,
                'trunk': '400 litros',
                'price_total': 430.15,
                'price_day': 76.87
            },
            'package_includes': [
                'KM Livre',
                'Proteção Pessoal',
                'Proteção contra colisão e dano',
                'Taxas locais',
                '1 Motorista Adicional'
            ],
            'offer_msg': 'Um condutor adicional incluso'
        },
        {
            'company_name': 'Hertz',
            'company_logo': 'https://moveuptogether.ca/wp-content/uploads/2006/01/hertz.jpg',
            'score': 4,
            'score_text': 'Muito Bom',
            'location': 'Balcão de Atendimento',
            'car': {
                'car_model': 'Ford Fiesta',
                'car_category': 'Categoria Minivan',
                'image': 'https://img.olx.com.br/images/94/942812010201344.jpg',
                'engine' : '1.8',
                'air' : 1,
                'shift': 'Manual',
                'electric_lock': 1,
                'abs': 1,
                'radio': 'CD Player USB',
                'passengers': 7,
                'electric_window': 1,
                'airbag': 1,
                'doors': 4,
                'trunk': '400 litros',
                'price_total': 230.27,
                'price_day': 76.87
            },
            'package_includes': [
                'KM Livre',
                'Proteção Pessoal',
                'Proteção contra colisão e dano',
                'Taxas locais',
                '1 Motorista Adicional'
            ],
            'offer_msg': 'Um condutor adicional incluso'
        },
        {
            'company_name': 'Hertz',
            'company_logo': 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQspVQ_Jy2Ap5oS7wswiV34O0ZOkBvK2FjCiSsNWsDrEVWzi1js',
            'score': 4,
            'score_text': 'Muito Bom',
            'location': 'Balcão de Atendimento',
            'car': {
                'car_model': 'Peugeot 208',
                'car_category': 'Categoria Minivan',
                'image': 'https://images.ouedkniss.com/automobiles/2414/Photo.jpg',
                'engine' : '1.8',
                'air' : 1,
                'shift': 'Manual',
                'electric_lock': 1,
                'abs': 1,
                'radio': 'CD Player USB',
                'passengers': 7,
                'electric_window': 1,
                'airbag': 1,
                'doors': 4,
                'trunk': '400 litros',
                'price_total': 230.85,
                'price_day': 76.87
            },
            'package_includes': [
                'KM Livre',
                'Proteção Pessoal',
                'Proteção contra colisão e dano',
                'Taxas locais',
                '1 Motorista Adicional'
            ],
            'offer_msg': 'Um condutor adicional incluso'
        },
        {
            'company_name': 'Hertz',
            'company_logo': 'https://moveuptogether.ca/wp-content/uploads/2006/01/hertz.jpg',
            'score': 4,
            'score_text': 'Muito Bom',
            'location': 'Balcão de Atendimento',
            'car': {
                'car_model': 'Fiat Palio',
                'car_category': 'Categoria Minivan',
                'image': 'http://4.bp.blogspot.com/_OWAbzfVagFo/S3oK4q0xo_I/AAAAAAAAE6I/oRgIxFSEQwI/s400/2003_Fiat_Palio_30.jpg',
                'engine' : '1.8',
                'air' : 1,
                'shift': 'Manual',
                'electric_lock': 1,
                'abs': 1,
                'radio': 'CD Player USB',
                'passengers': 7,
                'electric_window': 1,
                'airbag': 1,
                'doors': 4,
                'trunk': '400 litros',
                'price_total': 230.66,
                'price_day': 76.87
            },
            'package_includes': [
                'KM Livre',
                'Proteção Pessoal',
                'Proteção contra colisão e dano',
                'Taxas locais',
                '1 Motorista Adicional'
            ],
            'offer_msg': 'Um condutor adicional incluso'
        },
        {
            'company_name': 'Hertz',
            'company_logo': 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQspVQ_Jy2Ap5oS7wswiV34O0ZOkBvK2FjCiSsNWsDrEVWzi1js',
            'score': 4,
            'score_text': 'Muito Bom',
            'location': 'Balcão de Atendimento',
            'car': {
                'car_model': 'Renault Clio',
                'car_category': 'Categoria Minivan',
                'image': 'http://motosnovas.com.br/wp-content/uploads/2015/05/renault-clio-2016.jpg',
                'engine' : '1.8',
                'air' : 1,
                'shift': 'Manual',
                'electric_lock': 1,
                'abs': 1,
                'radio': 'CD Player USB',
                'passengers': 7,
                'electric_window': 1,
                'airbag': 1,
                'doors': 4,
                'trunk': '400 litros',
                'price_total': 230.68,
                'price_day': 76.87
            },
            'package_includes': [
                'KM Livre',
                'Proteção Pessoal',
                'Proteção contra colisão e dano',
                'Taxas locais',
                '1 Motorista Adicional'
            ],
            'offer_msg': 'Um condutor adicional incluso'
        },
        {
            'company_name': 'Hertz',
            'company_logo': 'https://moveuptogether.ca/wp-content/uploads/2006/01/hertz.jpg',
            'score': 4,
            'score_text': 'Muito Bom',
            'location': 'Balcão de Atendimento',
            'car': {
                'car_model': 'Fiat 500',
                'car_category': 'Categoria Minivan',
                'image': 'https://static.icarros.com/dbimg/imgadicionalanuncio/5/g119563_1',
                'engine' : '1.8',
                'air' : 1,
                'shift': 'Manual',
                'electric_lock': 1,
                'abs': 1,
                'radio': 'CD Player USB',
                'passengers': 7,
                'electric_window': 1,
                'airbag': 1,
                'doors': 4,
                'trunk': '400 litros',
                'price_total': 215.10,
                'price_day': 76.87
            },
            'package_includes': [
                'KM Livre',
                'Proteção Pessoal',
                'Proteção contra colisão e dano',
                'Taxas locais',
                '1 Motorista Adicional'
            ],
            'offer_msg': 'Um condutor adicional incluso'
        }
    ];

    mockdata = this.mockdataAll;

    ngOnInit() {
    }

}

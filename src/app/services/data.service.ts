import { UserData } from './../domain/user';
import { Injectable } from '@angular/core'
import { RentData } from './../domain/rent'
import { BehaviorSubject } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private rentDataSource = new BehaviorSubject(new RentData())
  rentData = this.rentDataSource.asObservable()

  private userDataSource = new BehaviorSubject(new UserData())
  userData = this.userDataSource.asObservable()

  constructor() { }

  setRentData(rentData: RentData) {
    this.rentDataSource.next(rentData)
  }

  setUserData(userData: UserData) {
    this.userDataSource.next(userData)
  }
}

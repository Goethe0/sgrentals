import { DataService } from './data.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as auth0 from 'auth0-js';
import Auth0Lock from "auth0-lock"
import { environment } from "../../environments/environment";;

//(window as any).global = window;

@Injectable({
    providedIn: 'root'
})
export class AuthService {

  public auth0Lock = new Auth0Lock(
    environment.auth.clientID,
    environment.auth.domain, {
      auth: {
        responseType: environment.auth.responseType,
        audience: environment.auth.audience,
        redirectUrl: environment.auth.redirectUri,
        redirect: false,
        params: {
          scope: environment.auth.scope
        }
      }
    }
  )

  constructor(private router: Router, private dataService: DataService) {
    this.auth0Lock.on('authenticated', (authResult: AuthResult) => {
      this.auth0Lock.getUserInfo(authResult.accessToken, (error, profile) => {
        if(error) {
          console.error('auth error', error)
          return
        }

        this.setSession(authResult, profile)
        this.dataService.setUserData({ username: profile.given_name || profile.nickname, email: profile.email })
        this.auth0Lock.hide()
      })
    })
  }

  public login(): void {
    this.auth0Lock.show();
  }

  public handleAuthentication(): void {
    let accessToken = localStorage.getItem('access_token'),
        idToken = localStorage.getItem('id_token'),
        profileGivenName = localStorage.getItem('profile_given_name'),
        profileEmail = localStorage.getItem('profile_email');

    if (accessToken && idToken && profileGivenName && profileEmail) {
      window.location.hash = '';
      this.dataService.setUserData({ username: profileGivenName, email: profileEmail });
    } else {
      console.log('not authenticated');
      // this.router.navigate(['/home'])
    }
  }

  public setSession(authResult: AuthResult, profile: auth0.Auth0UserProfile): void {
    const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);
    localStorage.setItem('profile_given_name', profile.given_name || profile.nickname);
    localStorage.setItem('profile_email', profile.email);
  }

  public logout(): void {
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    localStorage.removeItem('profile_given_name');
    localStorage.removeItem('profile_email');

    this.auth0Lock.logout({
      returnTo: '/'
    });
  }

  public isAuthenticated(): boolean {
    const expiresAt = JSON.parse(localStorage.getItem('expires_at') || '{}')
    return new Date().getTime() < expiresAt;
  }
}

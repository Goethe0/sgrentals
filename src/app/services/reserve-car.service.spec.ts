import { TestBed, inject } from '@angular/core/testing';

import { ReserveCarService } from './reserve-car.service';

describe('ReserveCarService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReserveCarService]
    });
  });

  it('should be created', inject([ReserveCarService], (service: ReserveCarService) => {
    expect(service).toBeTruthy();
  }));
});

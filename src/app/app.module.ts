import { AuthCallbackComponent } from './auth-callback/auth-callback.component';
import { RentCheckoutComponent } from './rent-checkout/rent-checkout.component';
import { CarListComponent } from './car-list/car-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
        MatDatepickerModule,
        MatNativeDateModule,
        MatButtonModule,
        MatRadioModule,
        MatCheckboxModule,
        MatSelectModule,
        MatIconModule } from "@angular/material";

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";


import { AppComponent } from './app.component';
import { ReserveCarUIComponent } from './components/reserve-car-ui/reserve-car-ui.component';
import { RentalStatePricesUIComponent } from './components/rental-state-prices-ui/rental-state-prices-ui.component';
import { HpHeaderComponent } from './components/hp-header/hp-header.component';
import { NewsletterSubscriptionUIComponent } from './components/newsletter-subscription-ui/newsletter-subscription-ui.component';
import { HpFooterUIComponent } from './components/hp-footer-ui/hp-footer-ui.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './/app-routing.module';
import { CarsListComponent } from './components/cars-list/cars-list.component';
import { MyAccountComponent } from './my-account/my-account.component';

import { LoginButtonComponent } from './components/login-button/login-button.component';
import { TestComponent } from './test/test.component';
import { CarSearchComponent } from './car-search/car-search.component';
import { YesNoDirective } from './yes-no.directive';
import { YesNoPipe } from './yes-no.pipe';


@NgModule({
  declarations: [
    AppComponent,
    ReserveCarUIComponent,
    RentalStatePricesUIComponent,
    HpHeaderComponent,
    NewsletterSubscriptionUIComponent,
    HpFooterUIComponent,
    CarListComponent,
    RentCheckoutComponent,
    HomeComponent,
    MyAccountComponent,
    CarsListComponent,
    AuthCallbackComponent,
    LoginButtonComponent,
    TestComponent,
    CarSearchComponent,
    YesNoDirective,
    YesNoPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,

    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MatCheckboxModule,
    MatRadioModule,
    MatSelectModule,
    MatIconModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

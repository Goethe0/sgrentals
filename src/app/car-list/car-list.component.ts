import { RentData } from './../domain/rent';
import { DataService } from './../services/data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-car-list',
  templateUrl: './car-list.component.html',
  styleUrls: ['./car-list.component.scss']
})
export class CarListComponent implements OnInit {

  constructor(private dataService: DataService) { }

  rentData: RentData

  ngOnInit() {
    this.dataService.rentData.subscribe(rentData => this.rentData = rentData)
  }


}

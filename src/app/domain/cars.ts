export interface CarData
{
  brand: string
  image: string
  name: string
  category: string
  daysRented: number
  details: string
  insurerLogo: string
  insurerName: string
  features: string[]
}
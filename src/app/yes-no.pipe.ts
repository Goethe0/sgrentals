import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'yesNo'
})
export class YesNoPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value === 1) {
      return 'Sim';
    } else {
      return 'Não';
    }
  }

}
